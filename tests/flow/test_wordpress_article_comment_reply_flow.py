import time
from base64 import b64encode
import requests
import pytest
import lorem

blog_url = 'https://gaworski.net'

username_author = 'editor'
password_author = 'HWZg hZIP jEfK XCDE V9WM PQ3t'
email_author = 'editor@somesite.com'
token_author = b64encode(f"{username_author}:{password_author}".encode('utf-8')).decode("ascii")

posts_endpoint_url = blog_url + "/wp-json/wp/v2/posts"

username_commenter = 'commenter'
password_commenter = 'SXlx hpon SR7k issV W2in zdTb'
email_commenter = 'commenter@somesite.com'
token_commenter = b64encode(f"{username_commenter}:{password_commenter}".encode('utf-8')).decode("ascii")

comments_endpoint_url = blog_url + "/wp-json/wp/v2/comments"


@pytest.fixture(scope='module')
def article():
    timestamp = int(time.time())
    article = {
        "article_creation_date": timestamp,
        "article_title": "This is new post by JPe" + str(timestamp),
        "article_subtitle": lorem.sentence(),
        "article_text": lorem.paragraph()
    }
    return article


@pytest.fixture(scope='module')
def headers_author():
    headers = {
        "Content-Type": "application/json",
        "Authorization": "Basic " + token_author
    }
    return headers


@pytest.fixture(scope='module')
def posted_article(article, headers_author):
    payload = {
        "title": article["article_title"],
        "excerpt": article["article_subtitle"],
        "content": article["article_text"],
        "status": "publish"
    }
    response = requests.post(url=posts_endpoint_url, headers=headers_author, json=payload)
    return response


@pytest.fixture(scope="module")
def comment():
    comment = {
        "author_name": "commenter",
        "author_email": email_commenter,
        "content": "This is content of the comment" + lorem.sentence()
    }
    return comment


@pytest.fixture(scope='module')
def headers_commenter():
    headers = {
        "Content-Type": "application/json",
        "Authorization": "Basic " + token_commenter
    }
    return headers


@pytest.fixture(scope="module")
def posted_comment(comment, headers_commenter, posted_article):
    post_id = posted_article.json()["id"]
    payload = {
        "author_name": comment["author_name"],
        "author_email": comment["author_email"],
        "content": comment["content"],
        "post": post_id,
        "status": "publish",
    }
    response = requests.post(url=comments_endpoint_url, headers=headers_commenter, json=payload)
    return response


@pytest.fixture(scope="module")
def response_author():
    comment = {
        "author_name": "editor",
        "author_email": email_author,
        "content": "This is response to the comment.  " + lorem.sentence()
    }
    return comment


@pytest.fixture(scope="module")
def posted_response_author(posted_comment, headers_author, response_author, posted_article):
    comment_id = posted_comment.json()["id"]
    post_id = posted_article.json()["id"]
    payload = {
        "author_name": response_author["author_name"],
        "author_email": response_author["author_email"],
        "content": response_author["content"],
        "parent": comment_id,
        "post": post_id,
        "status": "publish"
    }
    response = requests.post(url=comments_endpoint_url, headers=headers_author, json=payload)
    return response


def test_new_post_is_successfully_created(posted_article):
    assert posted_article.status_code == 201
    assert posted_article.reason == "Created"


def test_verify_author_of_post(article, posted_article):
    wordpress_post_id = posted_article.json()["id"]
    wordpress_post_url = f'{posts_endpoint_url}/{wordpress_post_id}'
    published_article = requests.get(url=wordpress_post_url)
    assert published_article.status_code == 200
    assert published_article.reason == "OK"
    wordpress_post_data = posted_article.json()
    wordpress_published_data = published_article.json()
    assert wordpress_post_data["author"] == wordpress_published_data["author"]


def test_new_comment_is_successfully_created(posted_comment, posted_article):
    assert posted_comment.status_code == 201
    assert posted_comment.reason == "Created"


def test_verify_relation_post_comment(posted_comment, posted_article):
    assert posted_comment.json()["post"] == posted_article.json()["id"]


def test_verify_author_of_comment(posted_comment, comment):
    wordpress_comment_url = f'{comments_endpoint_url}/{posted_comment.json()["id"]}'
    published_comment = requests.get(wordpress_comment_url)
    wordpress_comment_data = published_comment.json()
    assert published_comment.status_code == 200
    assert published_comment.reason == "OK"
    assert wordpress_comment_data["author_name"] == comment["author_name"]
    assert wordpress_comment_data["author"] == posted_comment.json()["author"]


def test_response_author_is_successfully_created(posted_response_author):
    assert posted_response_author.status_code == 201
    assert posted_response_author.reason == "Created"


def test_verify_relation_comment_response(posted_response_author, posted_comment):
    assert posted_response_author.json()["parent"] == posted_comment.json()["id"]


def test_verify_relation_response_post(posted_response_author, posted_article):
    assert posted_response_author.json()["post"] == posted_article.json()["id"]


def test_verify_author_of_response(response_author, posted_response_author):
    assert posted_response_author.json()["author_name"] == response_author["author_name"]

